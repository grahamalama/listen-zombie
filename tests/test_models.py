import pytest
from pydantic import ValidationError

from listen_zombie.models import (
    MAX_ITEMS_PER_GET,
    GetActivityParams,
    GetListensParams,
    GetTopEntitiesParams,
)


class Test_GetListensParams:
    def test_model_insantiation_min_ts(self):
        params = GetListensParams(count=100, min_ts=1)
        assert params.count == 100
        assert params.min_ts == 1

    def test_model_insantiation_max_ts(self):
        params = GetListensParams(max_ts=1)
        assert params.max_ts == 1

    def test_count_less_than_1(self):
        with pytest.raises(ValidationError):
            GetListensParams(count=0)

    def test_count_greater_than_max(self):
        with pytest.raises(ValidationError):
            GetListensParams(count=MAX_ITEMS_PER_GET + 1)

    def test_min_and_max_ts(self):
        with pytest.raises(ValidationError):
            assert GetListensParams(min_ts=1, max_ts=1)


class Test_GetActivityParams:
    def test_model_insantiation(self):
        params = GetActivityParams(range="week")
        assert params.range == "week"

    def test_invalid_range(self):
        with pytest.raises(ValidationError):
            GetActivityParams(range="weekly")


class Test_GetTopEntitiesParams:
    def test_model_insantiation(self):
        params = GetTopEntitiesParams(count=1, offset=1, range="week")
        assert params.count == 1
        assert params.offset == 1
        assert params.range == "week"

    def test_invalid_range(self):
        with pytest.raises(ValidationError):
            GetTopEntitiesParams(count=1, offset=1, range="weekly")
