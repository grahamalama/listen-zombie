import pytest
import responses

from listen_zombie.client import (
    get_artist_map,
    get_daily_activity,
    get_latest_import,
    get_listen_count,
    get_listening_activity,
    get_listens,
    get_playing_now,
    get_top_artists,
    get_top_recordings,
    get_top_releases,
)


@pytest.fixture
def mocked_responses():
    with responses.RequestsMock() as rsps:
        yield rsps


@pytest.fixture
def data():
    return {"some": "data"}


@pytest.fixture
def payload(data):
    return {"payload": data}


def test_get_listen_count(mocked_responses):
    mocked_responses.add(
        responses.GET,
        "https://api.listenbrainz.org/1/user/username/listen-count",
        json={"payload": {"count": 100}},
        status=200,
    )
    assert get_listen_count("username") == 100


class Test_get_playing_now:
    def test_listens_is_empty(self, mocked_responses, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/user/username/playing-now",
            json={"payload": {"listens": []}},
            status=200,
        )
        assert get_playing_now("username") is None

    def test_returns_listen(self, mocked_responses, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/user/username/playing-now",
            json={"payload": {"listens": [data]}},
            status=200,
        )
        assert get_playing_now("username") == data


class Test_get_listens:
    def test_returns_payload(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/user/username/listens",
            json=payload,
            status=200,
        )
        assert get_listens("username") == data

    def test_interpolates_url_params(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/user/username/listens?min_ts=1&count=1",
            json=payload,
            status=200,
        )
        assert get_listens("username", min_ts=1, count=1) == data


def test_get_latest_import(mocked_responses):
    mocked_responses.add(
        responses.GET,
        "https://api.listenbrainz.org/1/latest-import?user_name=username",
        json={"latest_import": 1000000000, "user_id": "username"},
        status=200,
    )
    assert get_latest_import("username") == 1000000000


class Test_get_listening_activity:
    def test_returns_payload(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/listening-activity",
            json=payload,
        )
        assert get_listening_activity("username") == data


class Test_daily_activity:
    def test_returns_payload(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/daily-activity",
            json={"payload": payload},
        )
        assert get_daily_activity("username") == payload


class Test_get_top_recordings:
    def test_returns_payload(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/recordings",
            json=payload,
            status=200,
        )
        assert get_top_recordings("username") == data

    def test_interpolates_url_params(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/recordings?range=week&count=10",
            json=payload,
            status=200,
        )
        assert get_top_recordings("username", range_="week", count=10) == data


class Test_get_top_releases:
    def test_returns_payload(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/releases",
            json=payload,
            status=200,
        )
        assert get_top_releases("username") == data

    def test_interpolates_url_params(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/releases?range=week&offset=10",
            json=payload,
            status=200,
        )
        assert get_top_releases("username", range_="week", offset=10) == data


class Test_get_top_artists:
    def test_returns_payload(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/artists",
            json=payload,
            status=200,
        )
        assert get_top_artists("username") == data

    def test_interpolates_url_params(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/artists?range=week&count=10",
            json=payload,
            status=200,
        )
        assert get_top_artists("username", range_="week", count=10) == data


class Test_get_artist_map:
    def test_returns_payload(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/artist-map",
            json=payload,
            status=200,
        )
        assert get_artist_map("username") == data

    def test_interpolates_url_params(self, mocked_responses, payload, data):
        mocked_responses.add(
            responses.GET,
            "https://api.listenbrainz.org/1/stats/user/username/artist-map?range=week&force_recalculate=True",
            json=payload,
            status=200,
        )
        assert get_artist_map("username", range_="week", force_recalculate=True) == data
