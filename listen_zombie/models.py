from enum import Enum
from typing import Optional

from pydantic import BaseModel, Field, root_validator

MAX_ITEMS_PER_GET = 100


class RangeEnum(str, Enum):
    week = "week"
    month = "month"
    year = "year"
    all_time = "all_time"


count_field = Field(None, ge=1, le=MAX_ITEMS_PER_GET)


class GetListensParams(BaseModel):
    min_ts: Optional[int]
    max_ts: Optional[int]
    count: Optional[int] = count_field

    @root_validator(pre=True)
    def check_timestamps(cls, values):
        assert not bool(
            values.get("min_ts") and values.get("max_ts")
        ), "You may specify max_ts or min_ts, but not both in one call."
        return values


class GetActivityParams(BaseModel):
    range: Optional[RangeEnum]


class GetTopEntitiesParams(BaseModel):
    count: Optional[int] = count_field
    offset: Optional[int]
    range: Optional[RangeEnum]


class GetArtistMapParams(BaseModel):
    range: Optional[RangeEnum]
    force_recalculate: Optional[bool]
